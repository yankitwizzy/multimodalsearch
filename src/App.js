import './App.css';
import {useState, useEffect} from "react";
import axios from 'axios';
import {TextField, InputAdornment, IconButton, ImageList, ImageListItem, Box, LinearProgress, ImageListItemBar } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';

const ETSY_WEB_API = "https://dev-ml-platform.etsycloud.com/barista/seldon-clip-green/predict";
const GET = 'get';

function App() {

    const [images, setImages] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [tempSearchTerm, setTempSearchTerm] = useState('');
    const [imageUrl, setImageUrl] = useState('');
    const [loading, setLoading] = useState(false);

    const processImages = (data) => {
        const imageUrls = [];
        if (data) {
            data.forEach(listingData => {
                // console.log(listingData);
                if (listingData?.data?.results) {
                    try {
                        let url = listingData?.data?.results[0]?.Images[0]?.url_570xN;
                        //url = url.replaceAll("full", "250");
                        imageUrls.push({
                            src: url,
                            width: 250,
                            height: 250,
                            title: listingData?.data?.results[0]?.title
                        });
                    } catch (e) {
                        console.log(`listing didn't have an image`, listingData.data);
                    }
                }
            });
        }
        return imageUrls;
    }

    const getEndpointAndData = (text, imgUrl, method, index) => {
        const params = {};
        if (text) {
            params.text = text;
        }

        if (imgUrl) {
            params.image_url = imgUrl;
        }

        return {
            url: ETSY_WEB_API,
            method: method || GET,
            data: {
                jsonData: {
                    ...params,
                    index_type: index || "joint",
                    num_neighbors: '50',
                    query_embedding_type: "avg_normalized_embeddings",
                }
            }
        };
    }

    const getListingImages = (listingIds) => {
        const requests = [];
        listingIds.forEach(listingId => {
            const url = `http://api-internal-0.etsy-web-prod.us-central1.etsycloud.com:7087/v2/internal/listings/${listingId}?includes=Images`;
            requests.push(axios.get(url));
        });

        axios.all(requests)
            .then(responses => {
                const imageUrls = processImages(responses);
                //console.log(imageUrls);
                setImages(imageUrls);
                //console.log("images", images);
                setLoading(false);
            })
    }

    // API call when component is mounted
    useEffect(() => {
        setLoading(true);
        axios(getEndpointAndData(
            'living room furniture',
            'https://i.etsystatic.com/5575667/r/il/0f0aae/1202272381/il_300x300.1202272381_ixps.jpg',
            'post'))
            .then(response => {
                //console.log(response?.data?.jsonData?.neighbors);
                getListingImages(response?.data?.jsonData?.neighbors || []);
            })
            .catch(error => {
                console.error('Error fetching data:', error);
                setLoading(false);
            });
    }, []);

    // API call when search term changes
    useEffect(() => {
        if (searchTerm) {
            setLoading(true);
            axios(getEndpointAndData(searchTerm, null, 'post', 'text'))
                .then(response => {
                    //console.log(response?.data?.jsonData?.neighbors);
                    getListingImages(response?.data?.jsonData?.neighbors || []);
                    //setSearchTerm('');
                    setImageUrl('');
                })
                .catch(error => {
                    // setSearchTerm('');
                    console.error('Error fetching data:', error);
                    setLoading(false);
                });
        }
    }, [searchTerm]);

    useEffect(() => {
        if (imageUrl) {
            setLoading(true);
            axios(getEndpointAndData(searchTerm, imageUrl, 'post'))
                .then(response => {
                    //console.log(response?.data?.jsonData?.neighbors);
                    getListingImages(response?.data?.jsonData?.neighbors || []);
                    setImageUrl('');
                })
                .catch(error => {
                    setImageUrl('');
                    console.error('Error fetching data:', error);
                    setLoading(false);
                });
        }
    }, [imageUrl]);

    const handleSelect = (src) => {
        if (src) {
            setImageUrl(src);
        }
    };

    const onSearch = (value) => {
        console.log("temp search term is ", tempSearchTerm);
        if (tempSearchTerm) {
            setSearchTerm(tempSearchTerm);
        }
    }

    const onChange = (e) => {
        //console.log(e.target.value);
        setTempSearchTerm(e.target.value);
        console.log(tempSearchTerm);
    }

    return (
        <div className="App">
            <div>
                <TextField disabled={loading} value={tempSearchTerm} onChange={onChange} fullWidth label="Search for anything"
                           InputProps={{
                               endAdornment: <InputAdornment position="end"><IconButton onClick={onSearch}><SearchIcon/></IconButton></InputAdornment>,
                           }}/>
            </div>
            {loading ? <Box sx={{ width: '100%' }}>
                <LinearProgress />
            </Box> : <div>
                <ImageList variant="masonry" cols={5} gap={0}>
                    {images.map((item) => (
                        <ImageListItem key={item.src}>
                            <img
                                src={`${item.src}?w=248&fit=crop&auto=format`}
                                srcSet={`${item.src}?w=248&fit=crop&auto=format&dpr=2 2x`}
                                alt={item.title}
                                loading="lazy"
                                onClick={() => handleSelect(item.src)}
                                className='listing-image'
                            />
                            <ImageListItemBar className="itembar"
                                title={item.title}
                                position="below"
                            />
                        </ImageListItem>
                    ))}
                </ImageList>
            </div>}
        </div>
    );
}

export default App;
